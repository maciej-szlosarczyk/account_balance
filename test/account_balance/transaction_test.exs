defmodule AccountBalance.TransactionTest do
  use ExUnit.Case, async: true

  alias AccountBalance.Transaction

  describe "decode_id/1" do
    test "returns back transaction map if it exists" do
      result = Transaction.decode_id("txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTIzfDE=")

      assert result == {:ok, %{account_id: "acc_test_account", date: ~D[2022-01-23], index: 1}}
    end

    test "returns error if does not parse" do
      result = Transaction.decode_id("acc_test_account")
      assert result == {:error, {:unknown_id_format, "acc_test_account"}}
    end
  end
end
