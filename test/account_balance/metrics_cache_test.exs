defmodule AccountBalance.MetricsCacheTest do
  use ExUnit.Case, async: true

  alias AccountBalance.MetricsCache

  setup do
    topic = make_ref() |> :erlang.term_to_binary() |> Base.encode64(padding: false)
    pid = start_supervised!({MetricsCache, [pubsub_topic: topic, skip_telemetry: true]})

    %{pid: pid, topic: topic}
  end

  describe "get_stats/1" do
    test "returns data", %{pid: pid} do
      expected_result = %{
        calls_by_account_id: %{
          "acc_g20AAAASMzM5ODU5NTF8NzE1MjM0MTQ1" => 0,
          "acc_g20AAAASNDQ3NTkwMjh8MTI2MDIwOTkz" => 0,
          "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1" => 0,
          "acc_g20AAAASNjgwMDczMTR8MjY4NDQ0MjU5" => 0
        },
        total_calls: 0
      }

      assert {:ok, expected_result} == MetricsCache.get_stats(pid)
    end
  end

  describe "pubsub integration" do
    setup(%{topic: topic}) do
      on_exit(fn ->
        :ok = Phoenix.PubSub.unsubscribe(AccountBalance.PubSub, topic)
      end)
    end

    test "updates stats", %{pid: pid, topic: topic} do
      account_id = make_ref() |> :erlang.term_to_binary() |> Base.encode64(padding: false)
      Phoenix.PubSub.subscribe(AccountBalance.PubSub, topic)
      Kernel.send(pid, {:increment, account_id})

      assert_receive {:stats_update,
                      %{
                        calls_by_account_id: %{^account_id => 1},
                        total_calls: 1
                      }}
    end
  end
end
