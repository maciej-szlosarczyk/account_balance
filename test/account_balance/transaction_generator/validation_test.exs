defmodule AccountBalance.TransactionGenerator.ValidationTest do
  use ExUnit.Case, async: true

  alias AccountBalance.CannedData
  alias AccountBalance.CannedData.Merchants, as: M
  alias AccountBalance.Fixtures
  alias AccountBalance.TransactionGenerator

  describe "validate/2" do
    test "validate real generator" do
      generator = CannedData.TransactionGenerators.approximately_real("acc_test")

      assert {:ok, generator} == TransactionGenerator.validate(generator)
    end

    test "returns error when all transactions do not sum up to 0" do
      generator = %AccountBalance.TransactionGenerator{
        account_id: "acc_test_account",
        start_date: Date.new!(2022, 01, 01),
        starting_balance: Decimal.new("0.00"),
        days: [
          [%{amount: Decimal.new("-3000.00"), counterparty: M.find_by_name("IKEA")}]
        ]
      }

      assert {:error, "days must reduce to 0, got: -3000.00"} ==
               TransactionGenerator.validate(generator)
    end

    test "validates also days with more than 1 transaction per day" do
      assert {:ok, Fixtures.nested_generator()} ==
               TransactionGenerator.validate(Fixtures.nested_generator())
    end

    test "returns {ok, generator} when generator conforms" do
      assert {:ok, Fixtures.flat_generator()} ==
               TransactionGenerator.validate(Fixtures.flat_generator())
    end
  end
end
