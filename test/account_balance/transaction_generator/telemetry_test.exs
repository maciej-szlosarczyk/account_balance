defmodule AccountBalance.TransactionGenerator.TelemetryTest do
  use ExUnit.Case, async: true

  alias AccountBalance.Fixtures
  alias AccountBalance.TransactionGenerator

  def attach_to_telemetry_events(events) do
    handler = fn event, measurements, metadata, _config ->
      send(
        self(),
        {:event_received, %{event: event, measurements: measurements, metadata: metadata}}
      )
    end

    handler_id = make_ref()

    if is_list(events) do
      :ok = :telemetry.attach_many(handler_id, events, handler, nil)
    else
      :ok = :telemetry.attach(handler_id, events, handler, nil)
    end

    ExUnit.Callbacks.on_exit(fn -> :telemetry.detach(handler_id) end)
  end

  setup do
    attach_to_telemetry_events([
      [:account_balance, :transaction_generator, :generate_transactions, :start],
      [:account_balance, :transaction_generator, :generate_transactions, :stop],
      [:account_balance, :transaction_generator, :generate_transactions, :exception]
    ])
  end

  test "creates telemetry events" do
    date = ~D[2022-01-23]
    generator = Fixtures.flat_generator()
    TransactionGenerator.generate_transactions(generator, date, date)

    assert_receive {:event_received,
                    %{
                      event: [
                        :account_balance,
                        :transaction_generator,
                        :generate_transactions,
                        :start
                      ],
                      metadata: %{
                        start_date: ^date,
                        end_date: ^date,
                        generator: ^generator
                      }
                    }}
  end
end
