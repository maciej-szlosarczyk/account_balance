defmodule AccountBalance.TransactionGeneratorTest do
  use ExUnit.Case, async: true

  alias AccountBalance.Fixtures
  alias AccountBalance.TransactionGenerator

  describe "generate_transactions/3" do
    test "generates many transactions for one day" do
      expected_result = Fixtures.transactions(:many)

      result =
        TransactionGenerator.generate_transactions(
          Fixtures.nested_generator(),
          ~D[2022-01-05],
          ~D[2022-01-01]
        )

      assert expected_result == result
    end

    test "generates transactions according to the excel" do
      expected_result = Fixtures.transactions(:excel)

      result =
        TransactionGenerator.generate_transactions(
          Fixtures.flat_generator(),
          ~D[2022-01-27],
          ~D[2022-01-20]
        )
        |> Enum.take(-8)

      assert expected_result == result
    end

    test "can be used to generate transaction for one day" do
      expected_result = [
        %AccountBalance.Transaction{
          account_id: "acc_test_account",
          amount: Decimal.new("-500.00"),
          date: ~D[2022-01-23],
          description: "IKEA",
          details: %{
            category: "home",
            counterparty: %{type: "organization", name: "IKEA"},
            processing_status: "complete"
          },
          id: "txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTIzfDE",
          running_balance: Decimal.new("2500.00"),
          status: "completed",
          type: "card_payment"
        }
      ]

      result =
        TransactionGenerator.generate_transactions(
          Fixtures.flat_generator(),
          ~D[2022-01-23],
          ~D[2022-01-23]
        )
        |> Enum.take(-1)

      assert expected_result == result
    end
  end
end
