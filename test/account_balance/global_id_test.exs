defmodule AccountBalance.GlobalIdTest do
  use ExUnit.Case, async: true
  use ExUnitProperties

  alias AccountBalance.GlobalId

  property "encode/2 and decode/2 are inverse" do
    check all type <- StreamData.one_of([:account, :transaction, :enrollment]),
              value <- StreamData.string(:alphanumeric) do
      {:ok, global_id} = GlobalId.encode(type, value)
      decoded = GlobalId.decode(global_id)

      assert {:ok, {type, value}} == decoded
    end
  end

  describe "encode/2" do
    test "accepts only selected types" do
      assert {:error, {:unsupported_value_type, :tomato}} == GlobalId.encode(:tomato, "")
    end
  end

  describe "decode/2" do
    test "returns error when type of value is unknown" do
      assert {:error, {:unknown_id_type, "tomato"}} == GlobalId.decode("tomato_1234")
    end

    test "returns error tuple when value cannot be converted to term" do
      maybe_global_id = ["acc", Base.encode64("Not a term")] |> Enum.join("_")
      assert {:error, {:error_decoding_id, "Not a term"}} == GlobalId.decode(maybe_global_id)
    end

    test "returns error tuple when value cannot be decoded with base64" do
      maybe_global_id = "acc_not_anid"
      assert {:error, {:unknown_id_format, "acc_not_anid"}} == GlobalId.decode(maybe_global_id)
    end
  end
end
