defmodule AccountBalance.CannedData.MerchantsTest do
  use ExUnit.Case, async: true

  alias AccountBalance.CannedData.Merchants

  describe "find_by_name/1" do
    test "returns nil when name is bogus" do
      assert nil == Merchants.find_by_name("bogus")
    end

    test "returns one merchant" do
      assert %{name: "IKEA", category: "home"} == Merchants.find_by_name("IKEA")
    end
  end

  describe "mark_as_income/1" do
    setup do
      %{merchant: %{name: "IKEA", category: "home"}}
    end

    test "returns a merchant", %{merchant: merchant} do
      assert %{name: "IKEA", category: "income"} == Merchants.mark_as_income(merchant)
    end
  end
end
