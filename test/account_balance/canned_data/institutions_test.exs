defmodule AccountBalance.CannedData.InstitutionsTest do
  use ExUnit.Case, async: true

  alias AccountBalance.CannedData.Institutions

  describe "find_by_name/1" do
    test "returns nil for bogus" do
      assert nil == Institutions.find_by_name("bogus")
    end

    test "returns institution for name" do
      assert %{name: "Chase", id: "chase"} == Institutions.find_by_name("Chase")
    end
  end

  describe "find_by_id/1" do
    test "returns nil for bogus" do
      assert nil == Institutions.find_by_id("bogus")
    end

    test "returns institution for id" do
      assert %{name: "Chase", id: "chase"} == Institutions.find_by_id("chase")
    end
  end
end
