defmodule AccountBalance.Fixtures do
  @moduledoc "Data to use in test to keep them leaner"

  alias AccountBalance.CannedData.Merchants, as: M
  alias AccountBalance.TransactionGenerator

  def flat_generator do
    income = M.find_by_name("Five Guys") |> M.mark_as_income()

    %TransactionGenerator{
      account_id: "acc_test_account",
      start_date: Date.new!(2022, 01, 01),
      starting_balance: Decimal.new("0.00"),
      days: [
        [%{amount: Decimal.new("3000.00"), counterparty: income}],
        [%{amount: Decimal.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: Decimal.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: Decimal.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: Decimal.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: Decimal.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: Decimal.new("-500.00"), counterparty: M.find_by_name("IKEA")}]
      ]
    }
  end

  def nested_generator do
    income = M.find_by_name("Five Guys") |> M.mark_as_income()

    %TransactionGenerator{
      account_id: "acc_nested_test_account",
      start_date: Date.new!(2022, 01, 01),
      starting_balance: Decimal.new("0.00"),
      days: [
        [%{amount: Decimal.new("3000.00"), counterparty: income}],
        [
          %{amount: Decimal.new("-1000.00"), counterparty: M.find_by_name("IKEA")},
          %{amount: Decimal.new("-1000.00"), counterparty: M.find_by_name("IKEA")},
          %{amount: Decimal.new("-1000.00"), counterparty: M.find_by_name("IKEA")}
        ],
        [],
        [%{amount: Decimal.new("3000.00"), counterparty: income}],
        [%{amount: Decimal.new("-3000.00"), counterparty: M.find_by_name("IKEA")}]
      ]
    }
  end

  def transaction_id(~D[2022-07-01]) do
    "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDctMDF8MQ"
  end

  def transaction_id(~D[2022-06-26]) do
    "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjZ8MQ"
  end

  def account_id do
    "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1"
  end

  def transaction(~D[2022-07-01]) do
    %{
      "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
      "running_balance" => "0.00",
      "amount" => "-500.00",
      "date" => "2022-07-01",
      "description" => "IKEA",
      "details" => %{
        "category" => "home",
        "counterparty" => %{"type" => "organization", "name" => "IKEA"},
        "processing_status" => "complete"
      },
      "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDctMDF8MQ",
      "links" => %{
        "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "self" =>
          "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDctMDF8MQ"
      },
      "status" => "completed",
      "type" => "card_payment"
    }
  end

  def transactions(:excel) do
    [
      %AccountBalance.Transaction{
        account_id: "acc_test_account",
        amount: Decimal.new("-500.00"),
        date: ~D[2022-01-20],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTIwfDE",
        running_balance: Decimal.new("500.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_test_account",
        amount: Decimal.new("-500.00"),
        date: ~D[2022-01-21],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTIxfDE",
        running_balance: Decimal.new("0.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_test_account",
        amount: Decimal.new("3000.00"),
        date: ~D[2022-01-22],
        description: "Five Guys",
        details: %{
          category: "income",
          counterparty: %{type: "organization", name: "Five Guys"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTIyfDE",
        running_balance: Decimal.new("3000.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_test_account",
        amount: Decimal.new("-500.00"),
        date: ~D[2022-01-23],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTIzfDE",
        running_balance: Decimal.new("2500.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_test_account",
        amount: Decimal.new("-500.00"),
        date: ~D[2022-01-24],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTI0fDE",
        running_balance: Decimal.new("2000.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_test_account",
        amount: Decimal.new("-500.00"),
        date: ~D[2022-01-25],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTI1fDE",
        running_balance: Decimal.new("1500.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_test_account",
        amount: Decimal.new("-500.00"),
        date: ~D[2022-01-26],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTI2fDE",
        running_balance: Decimal.new("1000.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_test_account",
        amount: Decimal.new("-500.00"),
        date: ~D[2022-01-27],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAdYWNjX3Rlc3RfYWNjb3VudHwyMDIyLTAxLTI3fDE",
        running_balance: Decimal.new("500.00"),
        status: "completed",
        type: "card_payment"
      }
    ]
  end

  def transactions(:many) do
    [
      %AccountBalance.Transaction{
        account_id: "acc_nested_test_account",
        amount: Decimal.new("3000.00"),
        date: ~D[2022-01-01],
        description: "Five Guys",
        details: %{
          category: "income",
          counterparty: %{type: "organization", name: "Five Guys"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAkYWNjX25lc3RlZF90ZXN0X2FjY291bnR8MjAyMi0wMS0wMXwx",
        running_balance: Decimal.new("3000.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_nested_test_account",
        amount: Decimal.new("-1000.00"),
        date: ~D[2022-01-02],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAkYWNjX25lc3RlZF90ZXN0X2FjY291bnR8MjAyMi0wMS0wMnwx",
        running_balance: Decimal.new("0.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_nested_test_account",
        amount: Decimal.new("-1000.00"),
        date: ~D[2022-01-02],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAkYWNjX25lc3RlZF90ZXN0X2FjY291bnR8MjAyMi0wMS0wMnwy",
        running_balance: Decimal.new("0.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_nested_test_account",
        amount: Decimal.new("-1000.00"),
        date: ~D[2022-01-02],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAkYWNjX25lc3RlZF90ZXN0X2FjY291bnR8MjAyMi0wMS0wMnwz",
        running_balance: Decimal.new("0.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_nested_test_account",
        amount: Decimal.new("3000.00"),
        date: ~D[2022-01-04],
        description: "Five Guys",
        details: %{
          category: "income",
          counterparty: %{type: "organization", name: "Five Guys"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAkYWNjX25lc3RlZF90ZXN0X2FjY291bnR8MjAyMi0wMS0wNHwx",
        running_balance: Decimal.new("3000.00"),
        status: "completed",
        type: "card_payment"
      },
      %AccountBalance.Transaction{
        account_id: "acc_nested_test_account",
        amount: Decimal.new("-3000.00"),
        date: ~D[2022-01-05],
        description: "IKEA",
        details: %{
          category: "home",
          counterparty: %{type: "organization", name: "IKEA"},
          processing_status: "complete"
        },
        id: "txn_g20AAAAkYWNjX25lc3RlZF90ZXN0X2FjY291bnR8MjAyMi0wMS0wNXwx",
        running_balance: Decimal.new("0.00"),
        status: "completed",
        type: "card_payment"
      }
    ]
  end

  def transactions(7) do
    [
      %{
        "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "amount" => "3000.00",
        "running_balance" => "3000.00",
        "date" => "2022-06-25",
        "description" => "Five Guys",
        "details" => %{
          "category" => "income",
          "counterparty" => %{"type" => "organization", "name" => "Five Guys"},
          "processing_status" => "complete"
        },
        "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjV8MQ",
        "links" => %{
          "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
          "self" =>
            "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjV8MQ"
        },
        "status" => "completed",
        "type" => "card_payment"
      },
      %{
        "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "amount" => "-500.00",
        "running_balance" => "2500.00",
        "date" => "2022-06-26",
        "description" => "IKEA",
        "details" => %{
          "category" => "home",
          "counterparty" => %{"type" => "organization", "name" => "IKEA"},
          "processing_status" => "complete"
        },
        "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjZ8MQ",
        "links" => %{
          "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
          "self" =>
            "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjZ8MQ"
        },
        "status" => "completed",
        "type" => "card_payment"
      },
      %{
        "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "amount" => "-500.00",
        "running_balance" => "2000.00",
        "date" => "2022-06-27",
        "description" => "IKEA",
        "details" => %{
          "category" => "home",
          "counterparty" => %{"type" => "organization", "name" => "IKEA"},
          "processing_status" => "complete"
        },
        "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjd8MQ",
        "links" => %{
          "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
          "self" =>
            "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjd8MQ"
        },
        "status" => "completed",
        "type" => "card_payment"
      },
      %{
        "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "amount" => "-500.00",
        "running_balance" => "1500.00",
        "date" => "2022-06-28",
        "description" => "IKEA",
        "details" => %{
          "category" => "home",
          "counterparty" => %{"type" => "organization", "name" => "IKEA"},
          "processing_status" => "complete"
        },
        "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjh8MQ",
        "links" => %{
          "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
          "self" =>
            "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjh8MQ"
        },
        "status" => "completed",
        "type" => "card_payment"
      },
      %{
        "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "amount" => "-500.00",
        "running_balance" => "1000.00",
        "date" => "2022-06-29",
        "description" => "IKEA",
        "details" => %{
          "category" => "home",
          "counterparty" => %{"type" => "organization", "name" => "IKEA"},
          "processing_status" => "complete"
        },
        "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjl8MQ",
        "links" => %{
          "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
          "self" =>
            "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjl8MQ"
        },
        "status" => "completed",
        "type" => "card_payment"
      },
      %{
        "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "amount" => "-500.00",
        "running_balance" => "500.00",
        "date" => "2022-06-30",
        "description" => "IKEA",
        "details" => %{
          "category" => "home",
          "counterparty" => %{"type" => "organization", "name" => "IKEA"},
          "processing_status" => "complete"
        },
        "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMzB8MQ",
        "links" => %{
          "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
          "self" =>
            "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMzB8MQ"
        },
        "status" => "completed",
        "type" => "card_payment"
      },
      %{
        "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "amount" => "-500.00",
        "running_balance" => "0.00",
        "date" => "2022-07-01",
        "description" => "IKEA",
        "details" => %{
          "category" => "home",
          "counterparty" => %{"type" => "organization", "name" => "IKEA"},
          "processing_status" => "complete"
        },
        "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDctMDF8MQ",
        "links" => %{
          "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
          "self" =>
            "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDctMDF8MQ"
        },
        "status" => "completed",
        "type" => "card_payment"
      }
    ]
  end

  def transactions(2) do
    [
      %{
        "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "amount" => "-500.00",
        "date" => "2022-06-24",
        "description" => "IKEA",
        "details" => %{
          "category" => "home",
          "counterparty" => %{"type" => "organization", "name" => "IKEA"},
          "processing_status" => "complete"
        },
        "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjR8MQ",
        "links" => %{
          "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
          "self" =>
            "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjR8MQ"
        },
        "status" => "completed",
        "type" => "card_payment",
        "running_balance" => "0.00"
      },
      %{
        "account_id" => "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
        "amount" => "3000.00",
        "date" => "2022-06-25",
        "description" => "Five Guys",
        "details" => %{
          "category" => "income",
          "counterparty" => %{"type" => "organization", "name" => "Five Guys"},
          "processing_status" => "complete"
        },
        "id" => "txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjV8MQ",
        "links" => %{
          "account" => "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1",
          "self" =>
            "http://localhost:4002/accounts/acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1/transactions/txn_g20AAAAxYWNjX2cyMEFBQUFTTlRjM056RXlNamQ4T1RNeU9EZ3dNamMxfDIwMjItMDYtMjV8MQ"
        },
        "status" => "completed",
        "type" => "card_payment",
        "running_balance" => "3000.00"
      }
    ]
  end
end
