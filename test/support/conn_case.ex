defmodule AccountBalanceWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use AccountBalanceWeb.ConnCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import AccountBalanceWeb.ConnCase
      alias AccountBalanceWeb.Router.Helpers, as: Routes

      alias AccountBalance.Fixtures

      # The default endpoint for testing
      @endpoint AccountBalanceWeb.Endpoint
    end
  end

  def assume_admin(%{conn: conn} = context) do
    user_data = {:user, "admin", "2022-07-01"}
    base = :erlang.term_to_binary(user_data) |> Base.encode64()
    user = "test_#{base}"

    conn =
      conn
      |> Plug.Conn.put_req_header("authorization", "Basic #{user}")

    %{context | conn: conn}
  end

  def assume_date(%{conn: conn, date: date} = context) do
    conn = Plug.Conn.put_private(conn, :"account_balance.assume_date", date)
    %{context | conn: conn}
  end

  def assume_first_of_july(%{conn: conn}) do
    assume_date(%{conn: conn, date: ~D[2022-07-01]})
  end

  setup _tags do
    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end
