defmodule AccountBalanceWeb.PageControllerTest do
  use AccountBalanceWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Users and API keys"

    assert html_response(conn, 200) =~
             "test_g2gDZAAEdXNlcm0AAAANYmFycmFja19vYmFtYW0AAAAKMjAyMi0wNy0wMQ"
  end
end
