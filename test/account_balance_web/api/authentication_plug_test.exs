defmodule AccountBalanceWeb.API.AuthenticationPlugTest do
  use AccountBalanceWeb.ConnCase, async: true

  alias AccountBalance.User
  alias AccountBalanceWeb.API.AuthenticationPlug

  def set_private_attribute(conn) do
    conn
    |> Plug.Conn.put_private(:"account_balance.skip_authentication", true)
  end

  describe "call/2" do
    test "bypasses authentication when private attribute is set", %{conn: conn} do
      conn = conn |> set_private_attribute() |> AuthenticationPlug.call()
      refute conn.halted
      assert conn.assigns[:current_user] == User.admin_user()
    end

    test "fails when no header is provided", %{conn: conn} do
      resp =
        Plug.Conn.put_resp_content_type(conn, "application/json")
        |> AuthenticationPlug.call()
        |> json_response(401)

      assert resp == %{"error" => "unathorized"}
    end

    test "succeeds when header is given", %{conn: conn} do
      user_data = {:user, "admin", "2022-07-01"}
      base = :erlang.term_to_binary(user_data) |> Base.encode64(padding: false)
      user = "test_#{base}"

      conn =
        conn
        |> Plug.Conn.put_req_header("authorization", "Basic #{user}")
        |> AuthenticationPlug.call()

      refute conn.halted
      assert conn.assigns[:current_user] == User.admin_user()
    end
  end
end
