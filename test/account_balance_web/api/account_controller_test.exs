defmodule AccountBalanceWeb.API.AccountControllerTest do
  use AccountBalanceWeb.ConnCase, async: true

  setup [:assume_admin, :assume_first_of_july]

  def expected_response do
    account_id = Fixtures.account_id()

    %{
      "currency" => "USD",
      "enrollment_id" => "enr_g20AAAASOTMyODgwMjc1fDU3NzcxMjI3",
      "id" => account_id,
      "institution" => %{"id" => "citibank", "name" => "Citibank"},
      "last_four" => "6732",
      "links" => %{
        "balances" => Routes.balance_url(@endpoint, :index, account_id),
        "details" => Routes.detail_url(@endpoint, :index, account_id),
        "self" => Routes.account_url(@endpoint, :show, account_id),
        "transactions" => Routes.transaction_url(@endpoint, :index, account_id)
      },
      "name" => "My Checking",
      "subtype" => "checking",
      "type" => "depository"
    }
  end

  describe "GET /" do
    test "returns expected JSON", %{conn: conn} do
      conn = get(conn, Routes.account_path(conn, :index))
      assert json_response(conn, 200) == [expected_response()]
    end
  end

  describe "GET /account_id" do
    test "returns single account resource", %{conn: conn} do
      conn = get(conn, Routes.account_path(conn, :show, Fixtures.account_id()))
      assert json_response(conn, 200) == expected_response()
    end

    test "returns 404 when account_id is not allowed", %{conn: conn} do
      conn = get(conn, Routes.account_path(conn, :show, "acc_made_up"))
      assert json_response(conn, 404) == %{"account_id" => "acc_made_up", "error" => "not found"}
    end
  end
end
