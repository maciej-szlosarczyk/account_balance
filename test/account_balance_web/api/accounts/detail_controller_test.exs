defmodule AccountBalanceWeb.API.Accounts.DetailControllerTest do
  use AccountBalanceWeb.ConnCase, async: true

  setup [:assume_admin]

  def account_id, do: "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1"

  def expected_response do
    %{
      "account_id" => account_id(),
      "account_number" => "57771227",
      "routing_numbers" => %{
        "ach" => "932880275"
      },
      "links" => %{
        "account" => Routes.account_url(@endpoint, :show, account_id()),
        "self" => Routes.detail_url(@endpoint, :index, account_id())
      }
    }
  end

  test "it returns expected JSON", %{conn: conn} do
    conn = get(conn, Routes.detail_path(conn, :index, account_id()))
    assert json_response(conn, 200) == expected_response()
  end

  test "it returns 404 when account_id is not known", %{conn: conn} do
    conn = get(conn, Routes.detail_path(conn, :index, "acc_made_up"))
    assert json_response(conn, 404) == %{"account_id" => "acc_made_up", "error" => "not found"}
  end
end
