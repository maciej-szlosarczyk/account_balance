defmodule AccountBalanceWeb.API.Accounts.TransactionControllerTest do
  use AccountBalanceWeb.ConnCase, async: true

  setup [:assume_admin, :assume_first_of_july]

  describe "GET /" do
    test "returns default JSON", %{conn: conn} do
      route = Routes.transaction_path(conn, :index, Fixtures.account_id(), count: 7)
      conn = get(conn, route)
      assert json_response(conn, 200) == Fixtures.transactions(7)
    end

    test "returns transaction for 90 days", %{conn: conn} do
      route = Routes.transaction_path(conn, :index, Fixtures.account_id())
      conn = get(conn, route)
      response = json_response(conn, 200)
      days = Enum.map(response, fn item -> Map.get(item, "date") end) |> Enum.uniq()
      first = List.first(days) |> Date.from_iso8601!()
      last = List.last(days) |> Date.from_iso8601!()

      assert Date.diff(last, first) == 90
    end

    test "with pagination", %{conn: conn} do
      route =
        Routes.transaction_path(conn, :index, Fixtures.account_id(),
          count: 2,
          from_id: Fixtures.transaction_id(~D[2022-06-26])
        )

      conn = get(conn, route)
      assert json_response(conn, 200) == Fixtures.transactions(2)
    end

    test "return 404 when account_id is not allowed", %{conn: conn} do
      route = Routes.transaction_path(conn, :index, "acc_made_up", count: 7)
      conn = get(conn, route)

      assert json_response(conn, 404) == %{"account_id" => "acc_made_up", "error" => "not found"}
    end
  end

  describe "for Jimmy Carter" do
    setup(context) do
      context =
        Map.put(context, :date, ~D[2022-07-03])
        |> assume_date()

      user_data = {:user, "jimmy_carter", "2022-07-01"}
      base = :erlang.term_to_binary(user_data) |> Base.encode64()
      user = "test_#{base}"

      conn =
        context.conn
        |> Plug.Conn.put_req_header("authorization", "Basic #{user}")

      %{context | conn: conn}
    end

    test "returns expected_response", %{conn: conn} do
      account_id = "acc_g20AAAASNDQ3NTkwMjh8MTI2MDIwOTkz"

      transaction_id =
        "txn_g20AAAAxYWNjX2cyMEFBQUFTTkRRM05Ua3dNamg4TVRJMk1ESXdPVGt6fDIwMjItMDQtMDd8MQ"

      expected_response = %{
        "account_id" => account_id,
        "id" => transaction_id,
        "links" => %{
          "account" => Routes.account_url(@endpoint, :show, account_id),
          "self" => Routes.transaction_url(@endpoint, :show, account_id, transaction_id)
        },
        "amount" => "-1000.00",
        "date" => "2022-04-07",
        "description" => "IKEA",
        "details" => %{
          "category" => "home",
          "counterparty" => %{"type" => "organization", "name" => "IKEA"},
          "processing_status" => "complete"
        },
        "status" => "completed",
        "type" => "card_payment",
        "running_balance" => "0.00"
      }

      conn = get(conn, Routes.transaction_path(conn, :show, account_id, transaction_id))
      assert json_response(conn, 200) == expected_response
    end
  end

  describe "GET /transaction_id" do
    test "it returns expected JSON", %{conn: conn} do
      route =
        Routes.transaction_path(
          conn,
          :show,
          Fixtures.account_id(),
          Fixtures.transaction_id(~D[2022-07-01])
        )

      conn = get(conn, route)

      assert json_response(conn, 200) == Fixtures.transaction(~D[2022-07-01])
    end

    test "return 404 when account_id is not allowed", %{conn: conn} do
      route =
        Routes.transaction_path(
          conn,
          :show,
          "acc_made_up",
          Fixtures.transaction_id(~D[2022-07-01])
        )

      conn = get(conn, route)

      assert json_response(conn, 404) == %{"account_id" => "acc_made_up", "error" => "not found"}
    end
  end
end
