defmodule AccountBalanceWeb.API.Accounts.BalanceControllerTest do
  use AccountBalanceWeb.ConnCase, async: true

  def account_id, do: "acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1"

  def expected_response do
    %{
      "account_id" => account_id(),
      "available" => "0.00",
      "ledger" => "500.00",
      "links" => %{
        "account" => Routes.account_url(@endpoint, :show, account_id()),
        "self" => Routes.balance_url(@endpoint, :index, account_id())
      }
    }
  end

  describe "with default expectations" do
    setup [:assume_admin, :assume_first_of_july]

    test "returns expected JSON", %{conn: conn} do
      conn = get(conn, Routes.balance_path(conn, :index, account_id()))
      assert json_response(conn, 200) == expected_response()
    end

    test "returns 404 when account_id is not known", %{conn: conn} do
      conn = get(conn, Routes.balance_path(conn, :index, "acc_made_up"))
      assert json_response(conn, 404) == %{"account_id" => "acc_made_up", "error" => "not found"}
    end
  end

  describe "for Jimmy Carter" do
    setup(context) do
      context =
        Map.put(context, :date, ~D[2022-07-03])
        |> assume_date()

      user_data = {:user, "jimmy_carter", "2022-07-01"}
      base = :erlang.term_to_binary(user_data) |> Base.encode64()
      user = "test_#{base}"

      conn =
        context.conn
        |> Plug.Conn.put_req_header("authorization", "Basic #{user}")

      %{context | conn: conn}
    end

    test "returns expected_response", %{conn: conn} do
      account_id = "acc_g20AAAASNDQ3NTkwMjh8MTI2MDIwOTkz"

      expected_response = %{
        "account_id" => account_id,
        "available" => "0.00",
        "ledger" => "1000.00",
        "links" => %{
          "account" => Routes.account_url(@endpoint, :show, account_id),
          "self" => Routes.balance_url(@endpoint, :index, account_id)
        }
      }

      conn = get(conn, Routes.balance_path(conn, :index, account_id))
      assert json_response(conn, 200) == expected_response
    end
  end

  describe "with positive transaction balance" do
    setup(context) do
      Map.put(context, :date, ~D[2022-07-23])
      |> assume_date()
      |> assume_admin()
    end

    test "returns expected response", %{conn: conn} do
      expected_response = %{
        "account_id" => account_id(),
        "available" => "3000.00",
        "ledger" => "0.00",
        "links" => %{
          "account" => Routes.account_url(@endpoint, :show, account_id()),
          "self" => Routes.balance_url(@endpoint, :index, account_id())
        }
      }

      conn = get(conn, Routes.balance_path(conn, :index, account_id()))
      assert json_response(conn, 200) == expected_response
    end
  end
end
