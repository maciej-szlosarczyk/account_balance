defmodule AccountBalanceWeb.MetricsLiveTest do
  use AccountBalanceWeb.ConnCase, async: true
  import Phoenix.LiveViewTest

  alias AccountBalanceWeb.MetricsLive

  setup(%{conn: conn}) do
    topic = make_ref() |> :erlang.term_to_binary() |> Base.encode64(padding: false)
    path = Routes.live_path(conn, MetricsLive, topic: topic)

    %{conn: conn, path: path, topic: topic}
  end

  test "renders HTML", %{conn: conn, path: path} do
    assert {:ok, _view, html} = live(conn, path)
    assert html =~ "Calls to TransactionGenerator.generate/3"
  end

  test "receives updates from messages", %{conn: conn, path: path} do
    assert {:ok, view, _html} = live(conn, path)

    message =
      {:stats_update, %{total_calls: 10, calls_by_account_id: %{"This-Is-A-Test-Account" => 10}}}

    send(view.pid, message)
    assert render(view) =~ "This-Is-A-Test-Account"
  end
end
