defmodule AccountBalance.Account do
  @moduledoc """
  All functionality for account.
  """

  alias AccountBalance.CannedData
  alias AccountBalance.TransactionGenerator

  defstruct [
    :currency,
    :id,
    :enrollment_id,
    :institution,
    :last_four,
    :name,
    :type,
    :number,
    :subtype,
    :routing_number,
    :transaction_generator
  ]

  @type t() :: %__MODULE__{
          currency: String.t(),
          id: String.t(),
          number: String.t(),
          enrollment_id: String.t(),
          institution: CannedData.Institutions.institution(),
          last_four: String.t(),
          name: String.t(),
          type: String.t(),
          subtype: String.t(),
          routing_number: String.t(),
          transaction_generator: TransactionGenerator.t()
        }

  @spec all() :: [t()]
  def all do
    CannedData.Accounts.all()
    |> Enum.map(&struct!(__MODULE__, &1))
  end

  def find_by_id(id), do: Enum.find(all(), &(&1.id == id))

  def find_by_ids(ids), do: Enum.filter(all(), &(&1.id in ids))
end
