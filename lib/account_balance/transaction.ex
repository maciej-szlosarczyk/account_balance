defmodule AccountBalance.Transaction do
  @moduledoc """
  The transaction struct, contains logic related to creating a transaction
  as well as the ability to generate global id out of transaction and find
  transaction details from a given id.
  """

  defstruct [
    :account_id,
    :amount,
    :date,
    :description,
    :details,
    :id,
    :status,
    :running_balance,
    :type
  ]

  @type t() :: %__MODULE__{
          account_id: String.t(),
          amount: Decimal.t(),
          date: Date.t(),
          description: String.t(),
          details: map(),
          id: String.t(),
          status: String.t(),
          running_balance: Decimal.t(),
          type: String.t()
        }

  def new(account_id, amount, date, counterparty, status, running_balance, index \\ 1) do
    %__MODULE__{
      account_id: account_id,
      amount: amount,
      date: date,
      description: counterparty.name,
      details: %{
        category: counterparty.category,
        counterparty: %{
          name: counterparty.name,
          type: "organization"
        },
        processing_status: "complete"
      },
      id: encode_id(account_id, date, index),
      running_balance: running_balance,
      status: status,
      type: "card_payment"
    }
  end

  def decode_id(transaction_id) do
    case AccountBalance.GlobalId.decode(transaction_id) do
      {:ok, {:transaction, tx}} ->
        [account_id, date, index] = String.split(tx, "|")
        {parsed_index, ""} = Integer.parse(index)
        map = %{account_id: account_id, date: Date.from_iso8601!(date), index: parsed_index}

        {:ok, map}

      error ->
        error
    end
  end

  defp encode_id(account_id, transaction_date, index) do
    base = [account_id, transaction_date, Integer.to_string(index)] |> Enum.join("|")
    {:ok, tx_id} = AccountBalance.GlobalId.encode(:transaction, base)
    tx_id
  end
end
