defmodule AccountBalance.User do
  @moduledoc """
  Look up users by their provided token. A user also has a list of hard-coded
  account ids they have access to.
  """

  defstruct [:name, :id, :accounts]

  def admin_user do
    %__MODULE__{name: "admin", id: "admin", accounts: ["acc_g20AAAASNTc3NzEyMjd8OTMyODgwMjc1"]}
  end

  def jimmy_carter do
    %__MODULE__{
      name: "Jimmy Carter",
      id: "jimmy_carter",
      accounts: ["acc_g20AAAASNDQ3NTkwMjh8MTI2MDIwOTkz"]
    }
  end

  def barrack_obama do
    %__MODULE__{
      name: "Barrack Obama",
      id: "barrack_obama",
      accounts: ["acc_g20AAAASNjgwMDczMTR8MjY4NDQ0MjU5"]
    }
  end

  def donald_trump do
    %__MODULE__{
      name: "Donald Trump",
      id: "donald_trump",
      accounts: ["acc_g20AAAASMzM5ODU5NTF8NzE1MjM0MTQ1"]
    }
  end

  def all_presidents do
    %__MODULE__{
      name: "All Accounts user",
      id: "all_accounts",
      accounts: [
        "acc_g20AAAASMzM5ODU5NTF8NzE1MjM0MTQ1",
        "acc_g20AAAASNjgwMDczMTR8MjY4NDQ0MjU5",
        "acc_g20AAAASNDQ3NTkwMjh8MTI2MDIwOTkz"
      ]
    }
  end

  def encode(user_token) do
    base = :erlang.term_to_binary(user_token) |> Base.encode64(padding: false)
    "test_#{base}"
  end

  def tokens do
    %{
      {:user, "donald_trump", "2022-07-01"} => donald_trump(),
      {:user, "barrack_obama", "2022-07-01"} => barrack_obama(),
      {:user, "jimmy_carter", "2022-07-01"} => jimmy_carter(),
      {:user, "admin", "2022-07-01"} => admin_user(),
      {:user, "all_presidents", "2022-07-01"} => all_presidents()
    }
  end

  def from_token(token) do
    Map.get(tokens(), token)
  end
end
