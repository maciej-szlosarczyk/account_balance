defmodule AccountBalance.MetricsCache do
  @moduledoc "Gathers updates from telemetry"

  use GenServer

  @events [
    [:account_balance, :transaction_generator, :generate_transactions, :start],
    [:account_balance, :transaction_generator, :generate_transactions, :stop],
    [:account_balance, :transaction_generator, :generate_transactions, :exception]
  ]

  @start_event [:account_balance, :transaction_generator, :generate_transactions, :start]

  @default_topic "transaction_generator_metrics"

  defstruct [:total_calls, :calls_by_account_id, :handler_name, :pubsub_topic]

  @type t() :: %__MODULE__{
          total_calls: non_neg_integer(),
          calls_by_account_id: map(),
          handler_name: String.t(),
          pubsub_topic: String.t()
        }

  def handle_telemetry_event(@start_event, _measurements, metadata, %{pid: pid}) do
    account_id = metadata.generator.account_id
    send(pid, {:increment, account_id})
    :ok
  end

  def handle_telemetry_event(_, _, _, _config) do
    :ok
  end

  @spec start_link(any()) :: {:ok, pid()}
  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, opts)
  end

  @impl GenServer
  @spec init(any()) :: {:ok, t()}
  def init(opts) do
    handler_name = opts[:handler_name] || {__MODULE__, make_ref()}
    config = %{pid: self()}
    pubsub_topic = opts[:pubsub_topic] || @default_topic

    unless opts[:skip_telemetry] do
      :ok =
        :telemetry.attach_many(
          handler_name,
          @events,
          &__MODULE__.handle_telemetry_event/4,
          config
        )
    end

    {:ok, default_state(handler_name, pubsub_topic)}
  end

  @impl GenServer
  def terminate(_reason, state) do
    :telemetry.detach(state.handler_name)
  end

  def get_stats(pid \\ __MODULE__) do
    GenServer.call(pid, :get_stats)
  end

  @impl GenServer
  def handle_call(:get_stats, _from, %__MODULE__{} = state) do
    stats = %{total_calls: state.total_calls, calls_by_account_id: state.calls_by_account_id}

    {:reply, {:ok, stats}, state}
  end

  @impl GenServer
  def handle_info(:notify_subscribers, %__MODULE__{} = state) do
    msg =
      {:stats_update,
       %{total_calls: state.total_calls, calls_by_account_id: state.calls_by_account_id}}

    Phoenix.PubSub.broadcast(AccountBalance.PubSub, state.pubsub_topic, msg)

    {:noreply, state}
  end

  def handle_info({:increment, account_id}, %__MODULE__{} = state) do
    new_counter_for_account_id = Map.update(state.calls_by_account_id, account_id, 1, &(&1 + 1))

    new_state =
      state
      |> Map.update!(:total_calls, &(&1 + 1))
      |> Map.put(:calls_by_account_id, new_counter_for_account_id)

    send(self(), :notify_subscribers)

    {:noreply, new_state}
  end

  defp default_state(handler_name, pubsub_topic) do
    account_ids = AccountBalance.Account.all() |> Enum.map(& &1.id)

    counters =
      for id <- account_ids, into: %{} do
        counter = 0
        {id, counter}
      end

    %__MODULE__{
      total_calls: 0,
      calls_by_account_id: counters,
      handler_name: handler_name,
      pubsub_topic: pubsub_topic
    }
  end
end
