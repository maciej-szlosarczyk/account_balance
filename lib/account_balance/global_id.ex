defmodule AccountBalance.GlobalId do
  @moduledoc """
  Generator of global ids, coming from Teller API. Given a supported
  type and term, returns back a ID to pass to customers.

  The IDs are unfortunately case-sensitive, but they since they are
  used as part of URLs (namely, paths), that is the only way for them
  to encode data without persistence.
  """

  @spec encode(atom(), term()) :: {:ok, String.t()} | {:error, term()}
  def encode(type, value) do
    with {:ok, prefix} <- type_to_prefix(type),
         suffix <- :erlang.term_to_binary(value),
         encoded_value <- Base.encode64(suffix, padding: false) do
      {:ok, Enum.join([prefix, encoded_value], "_")}
    end
  end

  @spec decode(String.t()) ::
          {:ok, {:account, any()} | {:enrollment, any()} | {:transaction, any()}}
          | {:error,
             {:error_decoding_id, binary()}
             | {:unknown_id_format, binary()}
             | {:unknown_id_type, binary()}}
  def decode(value) do
    with {:ok, {prefix, encoded_value}} <- split_id(value),
         {:ok, type} <- prefix_to_type(prefix),
         {:ok, base_decoded} <- decode_base64(encoded_value),
         {:ok, term} <- decode_binary(base_decoded) do
      {:ok, {type, term}}
    end
  end

  defp split_id(value) do
    case String.split(value, "_") do
      [prefix, encoded_value] -> {:ok, {prefix, encoded_value}}
      _ -> {:error, {:unknown_id_format, value}}
    end
  end

  defp decode_binary(value) do
    try do
      term = :erlang.binary_to_term(value)
      {:ok, term}
    rescue
      ArgumentError ->
        {:error, {:error_decoding_id, value}}
    end
  end

  defp decode_base64(value) do
    case Base.decode64(value, padding: false) do
      :error -> {:error, {:unknown_id_format, value}}
      {:ok, decoded} -> {:ok, decoded}
    end
  end

  defp type_to_prefix(:account), do: {:ok, "acc"}
  defp type_to_prefix(:transaction), do: {:ok, "txn"}
  defp type_to_prefix(:enrollment), do: {:ok, "enr"}
  defp type_to_prefix(type), do: {:error, {:unsupported_value_type, type}}

  defp prefix_to_type("acc"), do: {:ok, :account}
  defp prefix_to_type("txn"), do: {:ok, :transaction}
  defp prefix_to_type("enr"), do: {:ok, :enrollment}
  defp prefix_to_type(prefix), do: {:error, {:unknown_id_type, prefix}}
end
