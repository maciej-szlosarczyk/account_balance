defmodule AccountBalance.TransactionGenerator do
  @moduledoc """
  The transaction generator is a data structure that defines how to programatically generate
  transactions from a set date and balance given a list of transactions. It operates on an
  arbitrary principle which allows us to generate transactions far into the future without
  significant overhead.

    - The generator is always given an account id to be able to recreate any transaction, past or
      present it is queried about.
    - The generator is given a start date from which it will start generating and then repeating
      the list of transactions.
    - The list of transactions amounts __MUST__ reduce to 0 - this way we can make sure that the
      running amount does not do something weird such as grow boundlessly or drop below 0 for a
      very long time. It also allows us to skip in time to only generate the slice we are
      interested about.
  """

  @default_days -90
  @telemetry_prefix [:account_balance, :transaction_generator, :generate_transactions]

  defstruct([:account_id, :starting_balance, :start_date, :days])

  alias AccountBalance.CannedData.Merchants, as: M
  alias AccountBalance.Transaction

  @type t() :: %__MODULE__{
          account_id: String.t(),
          start_date: Date.t(),
          starting_balance: Decimal.t(),
          days: [[%{amount: Decimal.t(), counterparty: M.merchant()}]]
        }

  @spec one_for_day(t(), Date.t()) :: Transaction.t()
  def one_for_day(generator, day) do
    transactions = generate_transactions(generator, day, day)
    List.last(transactions)
  end

  @spec one_from_id_and_account(t(), String.t(), String.t()) :: Transaction.t()
  def one_from_id_and_account(generator, id, account_id) do
    with {:ok, transaction_data} <- Transaction.decode_id(id),
         :ok <- validate_same_account_id(transaction_data.account_id, account_id) do
      transactions =
        generate_transactions(generator, transaction_data.date, transaction_data.date)

      Enum.find(transactions, &(&1.id == id))
    end
  end

  def many_from_id_and_account(id, account, opts) do
    with {:ok, transaction_data} <- Transaction.decode_id(id),
         :ok <- validate_same_account_id(transaction_data.account_id, account.id) do
      start_date = Date.add(transaction_data.date, @default_days)

      transactions =
        generate_transactions(account.transaction_generator, transaction_data.date, start_date)
        |> Enum.reject(&(&1.id == id))

      if opts[:limit] do
        Enum.take(transactions, -opts[:limit])
      else
        transactions
      end
    end
  end

  def many_from_date(generator, end_date, opts) do
    start_date = Date.add(end_date, @default_days)
    transactions = generate_transactions(generator, end_date, start_date)

    if opts[:limit] do
      Enum.take(transactions, -opts[:limit])
    else
      transactions
    end
  end

  defp validate_same_account_id(this, other) do
    if this == other do
      :ok
    else
      {:error, :invalid_account_id}
    end
  end

  defp generate_days(needs_to_generate_days, elements, generator) do
    if needs_to_generate_days >= length(elements) do
      {:cont, elements ++ generator.days}
    else
      {:halt, elements}
    end
  end

  @spec generate_transactions(t(), Date.t(), Date.t()) :: [Transaction.t()]
  def generate_transactions(%__MODULE__{} = generator, end_date, start_date) do
    :telemetry.span(
      @telemetry_prefix,
      %{generator: generator, end_date: end_date, start_date: start_date},
      fn ->
        dates = generate_dates(end_date, start_date, generator.start_date, length(generator.days))

        # Generate a list of days by appending it to itself if needed
        transactions_to_generate =
          Enum.reduce_while(0..dates.needs_to_generate_days, generator.days, fn _, elements ->
            generate_days(dates.needs_to_generate_days, elements, generator)
          end)

        # Generate transactions with rolling balance
        {generated_transactions, _final_balance} =
          for tx <- transactions_to_generate, reduce: {[], generator.starting_balance} do
            {transactions, running_balance} ->
              day = Date.add(dates.new_start_date, length(transactions))
              zero = Decimal.new("0.00")

              subtotal =
                Enum.reduce(tx, zero, fn day, acc ->
                  Decimal.add(acc, day.amount)
                end)

              new_balance = Decimal.add(running_balance, subtotal)

              txs =
                tx
                |> Enum.with_index()
                |> Enum.map(fn {transaction, index} ->
                  Transaction.new(
                    generator.account_id,
                    transaction.amount,
                    day,
                    transaction.counterparty,
                    transaction[:status] || "completed",
                    new_balance,
                    index + 1
                  )
                end)

              new_transactions = transactions ++ [txs]
              {new_transactions, new_balance}
          end

        generated_transactions = List.flatten(generated_transactions)

        generated_transactions =
          Enum.filter(generated_transactions, fn x ->
            Date.compare(x.date, dates.end_date) in [:lt, :eq] and
              Date.compare(x.date, start_date) in [:gt, :eq]
          end)

        {generated_transactions, %{}}
      end
    )
  end

  @spec validate(t()) :: {:ok, t()} | {:error, term()}
  def validate(%__MODULE__{} = generator) do
    transaction_sum = Enum.reduce(generator.days, zero(), &sum_amounts/2)

    if transaction_sum != zero() do
      {:error, "days must reduce to 0, got: #{transaction_sum}"}
    else
      {:ok, generator}
    end
  end

  defp sum_amounts(day, acc) do
    if is_list(day) do
      sum = Enum.reduce(day, zero(), &sum_amounts/2)
      Decimal.add(acc, sum)
    else
      Decimal.add(acc, day.amount)
    end
  end

  defp zero, do: Decimal.new("0.00")

  defp generate_dates(
         end_date,
         start_date,
         original_start_date,
         transactions_until_repeat
       ) do
    closest_start_date =
      Date.range(original_start_date, start_date, transactions_until_repeat)
      |> Enum.to_list()
      |> List.last()

    new_start_date = closest_start_date || original_start_date
    need_to_generate_days = Date.diff(end_date, new_start_date)

    %{
      new_start_date: new_start_date,
      end_date: end_date,
      needs_to_generate_days: need_to_generate_days,
      original_start_date: original_start_date,
      feed_repeats: div(need_to_generate_days, transactions_until_repeat)
    }
  end
end
