defmodule AccountBalance.Balance do
  @moduledoc """
  Get balance for a day from an account definition.
  """

  alias AccountBalance.Account
  alias AccountBalance.TransactionGenerator

  defstruct [:account, :account_id, :ledger, :available]

  @type t() :: %__MODULE__{
          account: Account.t(),
          account_id: String.t(),
          ledger: Decimal.t(),
          available: Decimal.t()
        }

  @spec for_account_and_day(Account.t(), Date.t()) :: t()
  def for_account_and_day(%Account{} = account, date) do
    transaction = TransactionGenerator.one_for_day(account.transaction_generator, date)

    {ledger, available} =
      case Decimal.compare(transaction.amount, Decimal.new("0.00")) do
        :gt ->
          {Decimal.sub(transaction.running_balance, transaction.amount),
           transaction.running_balance}

        _x ->
          abs = Decimal.abs(transaction.amount)
          ledger = Decimal.add(abs, transaction.running_balance)

          {ledger, transaction.running_balance}
      end

    %__MODULE__{
      account: account,
      account_id: account.id,
      ledger: ledger,
      available: available
    }
  end
end
