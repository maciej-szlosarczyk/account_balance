defmodule AccountBalance.CannedData.TransactionGenerators do
  @moduledoc false

  alias AccountBalance.CannedData.Merchants, as: M
  alias AccountBalance.TransactionGenerator
  alias Decimal, as: D

  def approximately_real(account_id) do
    income = M.find_by_name("McDonald's") |> M.mark_as_income()

    %TransactionGenerator{
      account_id: account_id,
      start_date: Date.new!(2022, 01, 01),
      starting_balance: Decimal.new("1236.45"),
      days: [
        [%{amount: D.new("6666.00"), counterparty: income}],
        [%{amount: D.new("-100.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: D.new("-10.00"), counterparty: M.find_by_name("Spotify")}],
        [%{amount: D.new("-999.00"), counterparty: M.find_by_name("Apple")}],
        [%{amount: D.new("-99.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("CVS")}],
        [%{amount: D.new("-43.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("CVS")}],
        [%{amount: D.new("-50.45"), counterparty: M.find_by_name("McDonald's")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Walgreens")}],
        [],
        [%{amount: D.new("-56.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Burger King")}],
        [],
        [%{amount: D.new("-89.45"), counterparty: M.find_by_name("Best Buy")}],
        [%{amount: D.new("-100.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: D.new("-10.00"), counterparty: M.find_by_name("Spotify")}],
        [%{amount: D.new("-999.00"), counterparty: M.find_by_name("Apple")}],
        [%{amount: D.new("-99.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("CVS")}],
        [%{amount: D.new("-43.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("CVS")}],
        [%{amount: D.new("-50.45"), counterparty: M.find_by_name("McDonald's")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Walgreens")}],
        [],
        [%{amount: D.new("-56.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Burger King")}],
        [],
        [%{amount: D.new("-89.45"), counterparty: M.find_by_name("Amazon")}],
        [%{amount: D.new("-100.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: D.new("-10.00"), counterparty: M.find_by_name("Spotify")}],
        [%{amount: D.new("-999.00"), counterparty: M.find_by_name("Apple")}],
        [%{amount: D.new("-99.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("KFC")}],
        [%{amount: D.new("-43.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("KFC")}],
        [%{amount: D.new("-50.45"), counterparty: M.find_by_name("McDonald's")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Walgreens")}],
        [],
        [%{amount: D.new("-56.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Burger King")}],
        [],
        [%{amount: D.new("-89.45"), counterparty: M.find_by_name("Amazon")}],
        [%{amount: D.new("-89.45"), counterparty: M.find_by_name("Best Buy")}],
        [%{amount: D.new("-100.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: D.new("-10.00"), counterparty: M.find_by_name("Spotify")}],
        [%{amount: D.new("-999.00"), counterparty: M.find_by_name("Apple")}],
        [%{amount: D.new("-99.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("CVS")}],
        [%{amount: D.new("-43.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("CVS")}],
        [%{amount: D.new("-50.45"), counterparty: M.find_by_name("McDonald's")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Walgreens")}],
        [],
        [%{amount: D.new("-56.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Burger King")}],
        [],
        [
          %{amount: D.new("-89.45"), counterparty: M.find_by_name("Amazon")},
          %{amount: D.new("773.70"), counterparty: income},
          %{amount: D.new("-10.00"), counterparty: M.find_by_name("Spotify")}
        ],
        [%{amount: D.new("-999.00"), counterparty: M.find_by_name("Apple")}],
        [%{amount: D.new("-99.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("KFC")}],
        [%{amount: D.new("-43.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("KFC")}],
        [%{amount: D.new("-50.45"), counterparty: M.find_by_name("McDonald's")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Walgreens")}],
        [],
        [%{amount: D.new("-56.45"), counterparty: M.find_by_name("Walmart")}],
        [%{amount: D.new("-10.45"), counterparty: M.find_by_name("Burger King")}],
        [],
        [%{amount: D.new("-89.45"), counterparty: M.find_by_name("Amazon")}]
      ]
    }
  end

  def seven_days(account_id) do
    income = M.find_by_name("Five Guys") |> M.mark_as_income()

    %TransactionGenerator{
      account_id: account_id,
      start_date: Date.new!(2022, 01, 01),
      starting_balance: Decimal.new("0.00"),
      days: [
        [%{amount: D.new("3000.00"), counterparty: income}],
        [%{amount: D.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: D.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: D.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: D.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: D.new("-500.00"), counterparty: M.find_by_name("IKEA")}],
        [%{amount: D.new("-500.00"), counterparty: M.find_by_name("IKEA")}]
      ]
    }
  end

  def nested_three_days(account_id) do
    income = M.find_by_name("Five Guys") |> M.mark_as_income()

    %TransactionGenerator{
      account_id: account_id,
      start_date: Date.new!(2021, 1, 1),
      starting_balance: Decimal.new("0.00"),
      days: [
        [%{amount: D.new("3000.00"), counterparty: income}],
        [],
        [
          %{amount: D.new("-1000.00"), counterparty: M.find_by_name("IKEA")},
          %{amount: D.new("-1000.00"), counterparty: M.find_by_name("IKEA")},
          %{amount: D.new("-1000.00"), counterparty: M.find_by_name("IKEA")}
        ]
      ]
    }
  end
end
