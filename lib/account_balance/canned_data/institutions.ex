defmodule AccountBalance.CannedData.Institutions do
  @moduledoc """
  A list of institutions. Since this is a static list, there is no need
  to introduce a struct for it.
  """

  defstruct [:name, :id]

  @type institution() :: %{name: String.t(), id: String.t()}

  @spec all() :: [institution()]
  def all do
    [
      %{name: "Chase", id: "chase"},
      %{name: "Bank of America", id: "bank_of_america"},
      %{name: "Wells Fargo", id: "wells_fargo"},
      %{name: "Citibank", id: "citibank"},
      %{name: "Capital One", id: "capital_one"}
    ]
  end

  @spec find_by_name(String.t()) :: institution() | nil
  def find_by_name(name) do
    Enum.find(all(), fn institution -> institution.name == name end)
  end

  @spec find_by_id(String.t()) :: institution() | nil
  def find_by_id(id) do
    Enum.find(all(), fn institution -> institution.id == id end)
  end
end
