defmodule AccountBalance.CannedData.Merchants do
  @moduledoc """
  A list of merchants and the categories they correspond to. Since this is a static list, there
  is no need to introduce a struct for it.

  The categories often do not correspond with what the companies really do, but it is not that
  important for the quality of the task.
  """

  @type merchant() :: %{name: String.t(), category: String.t()}

  def categories do
    [
      "accommodation",
      "advertising",
      "bar",
      "charity",
      "clothing",
      "dining",
      "education",
      "entertainment",
      "fuel",
      "groceries",
      "health",
      "home",
      "income",
      "insurance",
      "office",
      "phone",
      "service",
      "shopping",
      "software",
      "sport",
      "tax",
      "transport",
      "utilities"
    ]
  end

  @spec all() :: [merchant()]
  def all do
    [
      %{name: "Uber", category: "transport"},
      %{name: "Uber Eats", category: "dining"},
      %{name: "Lyft", category: "transport"},
      %{name: "Five Guys", category: "dining"},
      %{name: "In-N-Out Burger", category: "dining"},
      %{name: "Chick-Fil-A", category: "dining"},
      %{name: "Apple", category: "software"},
      %{name: "Amazon", category: "shopping"},
      %{name: "Walmart", category: "shopping"},
      %{name: "Target", category: "shopping"},
      %{name: "Hotel Tonight", category: "accomodation"},
      %{name: "Misson Ceviche", category: "dining"},
      %{name: "Caltrain", category: "transport"},
      %{name: "Wingstop", category: "dining"},
      %{name: "Slim Chickens", category: "dining"},
      %{name: "CVS", category: "health"},
      %{name: "Duane Reade", category: "health"},
      %{name: "Walgreens", category: "health"},
      %{name: "McDonald's", category: "dining"},
      %{name: "Burger King", category: "dining"},
      %{name: "KFC", category: "transport"},
      %{name: "Popeye\'s", category: "transport"},
      %{name: "Shake Shack", category: "transport"},
      %{name: "Lowe\'s", category: "transport"},
      %{name: "Costco", category: "shopping"},
      %{name: "Kroger", category: "transport"},
      %{name: "iTunes", category: "entertainment"},
      %{name: "Spotify", category: "entertainment"},
      %{name: "Best Buy", category: "shopping"},
      %{name: "TJ Maxx", category: "clothing"},
      %{name: "Aldi", category: "groceries"},
      %{name: "Macy\'s", category: "transport"},
      %{name: "H.E. Butt", category: "tax"},
      %{name: "Dollar Tree", category: "shopping"},
      %{name: "Verizon Wireless", category: "phone"},
      %{name: "Sprint PCS", category: "phone"},
      %{name: "Starbucks", category: "transport"},
      %{name: "7-Eleven", category: "groceries"},
      %{name: "AT&T Wireless", category: "phone"},
      %{name: "Rite Aid", category: "transport"},
      %{name: "Nordstrom", category: "transport"},
      %{name: "Ross", category: "transport"},
      %{name: "Bed, Bath & Beyond", category: "home"},
      %{name: "J.C. Penney", category: "fashion"},
      %{name: "Subway", category: "dining"},
      %{name: "O\'Reilly", category: "education"},
      %{name: "Wendy\'s", category: "dining"},
      %{name: "Petsmart", category: "home"},
      %{name: "Dick's Sporting Goods", category: "sport"},
      %{name: "Sears", category: "transport"},
      %{name: "Staples", category: "transport"},
      %{name: "Domino's Pizza", category: "transport"},
      %{name: "Papa John's", category: "transport"},
      %{name: "IKEA", category: "home"},
      %{name: "Office Depot", category: "transport"},
      %{name: "Foot Locker", category: "clothing"},
      %{name: "Lids", category: "transport"},
      %{name: "GameStop", category: "entertainment"},
      %{name: "Panera", category: "transport"},
      %{name: "Williams-Sonoma", category: "tax"},
      %{name: "Saks Fifth Avenue", category: "sport"},
      %{name: "Chipotle Mexican Grill", category: "dining"},
      %{name: "Neiman Marcus", category: "transport"},
      %{name: "Jack In The Box", category: "dining"},
      %{name: "Sonic", category: "transport"},
      %{name: "Shell", category: "fuel"}
    ]
  end

  @spec find_by_name(String.t()) :: merchant()
  def find_by_name(name) do
    Enum.find(all(), fn merchant -> merchant.name == name end)
  end

  @spec mark_as_income(merchant()) :: merchant()
  def mark_as_income(merchant) do
    Map.put(merchant, :category, "income")
  end
end
