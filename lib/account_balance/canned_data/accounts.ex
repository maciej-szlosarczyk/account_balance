defmodule AccountBalance.CannedData.Accounts do
  @moduledoc false

  alias AccountBalance.CannedData

  # These functions return bare maps to not create a compile time cycle with
  # AccountBalance.Account module.
  def all do
    [
      account("my_checking"),
      account("jimmy_carter"),
      account("barrack_obama"),
      account("donald_trump")
    ]
  end

  def account("my_checking") do
    number = "57771227"
    routing_number = "932880275"
    id = generate_id(:account, number, routing_number)
    enrollment_id = generate_id(:enrollment, number, routing_number)

    %{
      currency: "USD",
      id: id,
      enrollment_id: enrollment_id,
      routing_number: routing_number,
      number: number,
      institution: CannedData.Institutions.find_by_id("citibank"),
      last_four: "6732",
      name: "My Checking",
      type: "depository",
      subtype: "checking",
      transaction_generator: CannedData.TransactionGenerators.seven_days(id)
    }
  end

  def account("barrack_obama") do
    number = "68007314"
    routing_number = "268444259"
    id = generate_id(:account, number, routing_number)
    enrollment_id = generate_id(:enrollment, number, routing_number)

    %{
      currency: "USD",
      id: id,
      enrollment_id: enrollment_id,
      routing_number: routing_number,
      number: number,
      institution: CannedData.Institutions.find_by_id("wells_fargo"),
      last_four: "6732",
      name: "Barrack Obama",
      type: "depository",
      subtype: "checking",
      transaction_generator: CannedData.TransactionGenerators.approximately_real(id)
    }
  end

  def account("donald_trump") do
    number = "33985951"
    routing_number = "715234145"
    id = generate_id(:account, number, routing_number)
    enrollment_id = generate_id(:enrollment, number, routing_number)

    %{
      currency: "USD",
      id: id,
      enrollment_id: enrollment_id,
      routing_number: routing_number,
      number: number,
      institution: CannedData.Institutions.find_by_id("capital_one"),
      last_four: "6732",
      name: "Donald Trump",
      type: "depository",
      subtype: "checking",
      transaction_generator: CannedData.TransactionGenerators.approximately_real(id)
    }
  end

  def account("jimmy_carter") do
    number = "44759028"
    routing_number = "126020993"
    id = generate_id(:account, number, routing_number)
    enrollment_id = generate_id(:enrollment, number, routing_number)

    %{
      currency: "USD",
      id: id,
      enrollment_id: enrollment_id,
      routing_number: routing_number,
      number: number,
      institution: CannedData.Institutions.find_by_id("bank_of_america"),
      last_four: "3478",
      name: "Jimmy Carter",
      type: "depository",
      subtype: "checking",
      transaction_generator: CannedData.TransactionGenerators.nested_three_days(id)
    }
  end

  defp generate_id(:account, number, routing_number) do
    base = [number, routing_number] |> Enum.join("|")
    {:ok, account_id} = AccountBalance.GlobalId.encode(:account, base)
    account_id
  end

  defp generate_id(:enrollment, number, routing_number) do
    base = [routing_number, number] |> Enum.join("|")
    {:ok, account_id} = AccountBalance.GlobalId.encode(:enrollment, base)
    account_id
  end
end
