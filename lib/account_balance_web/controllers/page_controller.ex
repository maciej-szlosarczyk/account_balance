defmodule AccountBalanceWeb.PageController do
  use AccountBalanceWeb, :controller

  def index(conn, _params) do
    users =
      AccountBalance.User.tokens()
      |> Enum.map(fn {key, _value} ->
        {:user, name, _date} = key
        token = AccountBalance.User.encode(key)

        {name, token}
      end)

    render(conn, "index.html", users: users)
  end
end
