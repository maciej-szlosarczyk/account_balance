defmodule AccountBalanceWeb.MetricsLive do
  @moduledoc "Metrics page"
  use AccountBalanceWeb, :live_view

  alias Phoenix.LiveView

  @default_topic "transaction_generator_metrics"

  @impl LiveView
  def mount(params, _session, socket) do
    topic = Map.get(params, "topic", @default_topic)

    socket =
      socket
      |> LiveView.assign(:topic, topic)
      |> LiveView.assign(:total, 0)
      |> LiveView.assign(:by_account_id, %{})

    Process.send_after(self(), :subscribe, 100)

    {:ok, socket}
  end

  @impl Phoenix.LiveView
  def handle_info(:subscribe, socket) do
    topic = socket.assigns.topic

    :ok = Phoenix.PubSub.subscribe(AccountBalance.PubSub, topic)
    {:ok, stats} = AccountBalance.MetricsCache.get_stats()

    socket =
      socket
      |> LiveView.assign(:total, stats.total_calls)
      |> LiveView.assign(:by_account_id, stats.calls_by_account_id)

    {:noreply, socket}
  end

  def handle_info({:stats_update, stats}, socket) do
    socket =
      socket
      |> LiveView.assign(:total, stats.total_calls)
      |> LiveView.assign(:by_account_id, stats.calls_by_account_id)

    {:noreply, socket}
  end

  @impl Phoenix.LiveView
  def render(assigns) do
    ~H"""
    <table>
      <thead>
        <tr>
          <th>Account ID</th>
          <th>Calls to TransactionGenerator.generate/3</th>
        </tr>
      </thead>
      <tbody>
      <%= for {key, value} <- @by_account_id do %>
        <tr>
          <td><%= key %></td>
          <td><%= value %></td>
        </tr>
        <% end %>
        <tr>
          <td>TOTAL</td>
          <td><%= @total %></td>
        </tr>
      </tbody>
    </table>
    """
  end

  @impl Phoenix.LiveView
  def terminate(_reason, socket) do
    topic = socket.assigns.topic
    :ok = Phoenix.PubSub.unsubscribe(AccountBalance.PubSub, topic)
  end
end
