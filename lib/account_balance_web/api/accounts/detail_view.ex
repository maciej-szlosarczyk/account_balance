defmodule AccountBalanceWeb.API.Accounts.DetailView do
  use AccountBalanceWeb, :view

  def render("index.json", %{account: account}) do
    %{
      "account_id" => account.id,
      "account_number" => account.number,
      "routing_numbers" => %{"ach" => account.routing_number},
      "links" => %{
        "account" => Routes.account_url(AccountBalanceWeb.Endpoint, :show, account.id),
        "self" => Routes.detail_url(AccountBalanceWeb.Endpoint, :index, account.id)
      }
    }
  end
end
