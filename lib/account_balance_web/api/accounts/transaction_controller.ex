defmodule AccountBalanceWeb.API.Accounts.TransactionController do
  use AccountBalanceWeb, :controller

  alias AccountBalance.Account
  alias AccountBalance.TransactionGenerator

  alias Plug.Conn

  @upper_pagination_limit 90

  def index(conn, %{"account_id" => account_id} = params) do
    account_ids = conn.assigns.accounts

    if account_id in account_ids do
      count = Access.get(params, "count", nil)
      current_date = conn.assigns.current_date
      from_id = Access.get(params, "from_id")
      {:ok, count} = parse_count(count)

      account = Account.find_by_id(account_id)

      transactions =
        if from_id do
          TransactionGenerator.many_from_id_and_account(from_id, account, limit: count)
        else
          TransactionGenerator.many_from_date(account.transaction_generator, current_date,
            limit: count
          )
        end

      conn
      |> Conn.put_status(200)
      |> render("index.json", transactions: transactions)
    else
      conn
      |> Conn.put_status(404)
      |> json(%{error: "not found", account_id: account_id})
    end
  end

  def show(conn, %{"account_id" => account_id, "transaction_id" => transaction_id}) do
    account_ids = conn.assigns.accounts

    if account_id in account_ids do
      account = Account.find_by_id(account_id)

      transaction =
        TransactionGenerator.one_from_id_and_account(
          account.transaction_generator,
          transaction_id,
          account.id
        )

      conn
      |> Conn.put_status(200)
      |> render("show.json", transaction: transaction)
    else
      conn
      |> Conn.put_status(404)
      |> json(%{error: "not found", account_id: account_id})
    end
  end

  defp parse_count(nil) do
    {:ok, nil}
  end

  defp parse_count(str) do
    case Integer.parse(str) do
      {count, ""} -> {:ok, min(count, @upper_pagination_limit)}
      :error -> {:error, "Error parsing count parameter"}
    end
  end
end
