defmodule AccountBalanceWeb.API.Accounts.DetailController do
  use AccountBalanceWeb, :controller

  alias Plug.Conn

  def index(conn, %{"account_id" => account_id}) do
    account_ids = conn.assigns.accounts

    if account_id in account_ids do
      account = AccountBalance.Account.find_by_id(account_id)
      render(conn, "index.json", account: account)
    else
      conn
      |> Conn.put_status(404)
      |> json(%{"error" => "not found", "account_id" => account_id})
    end
  end
end
