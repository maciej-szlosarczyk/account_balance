defmodule AccountBalanceWeb.API.Accounts.BalanceController do
  use AccountBalanceWeb, :controller

  alias Plug.Conn

  alias AccountBalance.Balance

  def index(conn, %{"account_id" => account_id}) do
    account_ids = conn.assigns.accounts

    if account_id in account_ids do
      current_date = conn.assigns.current_date
      account = AccountBalance.Account.find_by_id(account_id)
      balance = Balance.for_account_and_day(account, current_date)

      render(conn, "index.json", balance: balance)
    else
      conn
      |> Conn.put_status(404)
      |> json(%{error: "not found", account_id: account_id})
    end
  end
end
