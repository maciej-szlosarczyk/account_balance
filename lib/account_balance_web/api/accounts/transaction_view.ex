defmodule AccountBalanceWeb.API.Accounts.TransactionView do
  use AccountBalanceWeb, :view

  def render("index.json", %{transactions: transactions}) do
    render_many(transactions, __MODULE__, "show.json")
  end

  def render("show.json", %{transaction: transaction}) do
    %{
      "account_id" => transaction.account_id,
      "amount" => transaction.amount,
      "date" => transaction.date,
      "description" => transaction.description,
      "details" => transaction.details,
      "id" => transaction.id,
      "links" => %{
        "account" =>
          Routes.account_url(AccountBalanceWeb.Endpoint, :show, transaction.account_id),
        "self" =>
          Routes.transaction_url(
            AccountBalanceWeb.Endpoint,
            :show,
            transaction.account_id,
            transaction.id
          )
      },
      "status" => transaction.status,
      "running_balance" => transaction.running_balance,
      "type" => transaction.type
    }
  end
end
