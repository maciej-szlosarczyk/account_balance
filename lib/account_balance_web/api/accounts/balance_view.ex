defmodule AccountBalanceWeb.API.Accounts.BalanceView do
  use AccountBalanceWeb, :view

  def render("index.json", %{balance: balance}) do
    %{
      "account_id" => balance.account_id,
      "available" => balance.available,
      "ledger" => balance.ledger,
      "links" => %{
        "account" => Routes.account_url(AccountBalanceWeb.Endpoint, :show, balance.account.id),
        "self" => Routes.balance_url(AccountBalanceWeb.Endpoint, :index, balance.account.id)
      }
    }
  end
end
