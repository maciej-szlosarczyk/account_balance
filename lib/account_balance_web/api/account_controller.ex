defmodule AccountBalanceWeb.API.AccountController do
  use AccountBalanceWeb, :controller

  alias Plug.Conn

  def index(conn, _) do
    account_ids = conn.assigns.accounts
    accounts = AccountBalance.Account.find_by_ids(account_ids)

    conn
    |> Conn.put_status(200)
    |> render("index.json", accounts: accounts)
  end

  def show(conn, %{"account_id" => account_id}) do
    account_ids = conn.assigns.current_user.accounts

    if account_id in account_ids do
      account = AccountBalance.Account.find_by_id(account_id)

      conn
      |> Conn.put_status(200)
      |> render("show.json", account: account)
    else
      conn
      |> Conn.put_status(404)
      |> json(%{error: "not found", account_id: account_id})
    end
  end
end
