defmodule AccountBalanceWeb.API.AuthenticationPlug do
  @moduledoc """
  Authentication plug implementing the scheme that Teller designed. It uses
  the `Basic` prefix, but not the basic scheme - i.e no password is sent.

  A token looks like this, and it is case-sensitive.

  ```
  "test_g2gDZAAEdXNlcm0AAAAFYWRtaW5tAAAACjIwMjItMDctMDE="
  ```

  It encodes user name and a date so that tokens can be removed based on days or username or both.

  ```
  {:user, "admin", "2022-07-01"}
  ```
  """

  @behaviour Plug
  alias Plug.Conn

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  def call(conn, opts \\ [])

  def call(%Conn{private: %{"account_balance.skip_authentication": true}} = conn, _opts) do
    user = AccountBalance.User.admin_user()

    conn
    |> Conn.assign(:current_user, user)
    |> Conn.assign(:accounts, user.accounts)
  end

  def call(conn, _opts) do
    with [header] <- Plug.Conn.get_req_header(conn, "authorization"),
         {:ok, header_value} <- match_header_value(header),
         {:ok, username} <- match_username(header_value),
         {:ok, user} <- get_user(username) do
      conn
      |> Conn.assign(:current_user, user)
      |> Conn.assign(:accounts, user.accounts)
    else
      _result ->
        conn
        |> Plug.Conn.send_resp(401, Jason.encode!(%{error: :unathorized}))
        |> Plug.Conn.halt()
    end
  end

  defp match_header_value(<<"Basic test_", user_name::binary>>) do
    {:ok, user_name}
  end

  defp match_header_value(any), do: {:error, any}

  defp match_username(user_name) do
    result = Base.decode64!(user_name, padding: false) |> :erlang.binary_to_term()
    {:ok, result}
  end

  def get_user(token) do
    case AccountBalance.User.from_token(token) do
      nil ->
        {:error, nil}

      user ->
        {:ok, user}
    end
  end
end
