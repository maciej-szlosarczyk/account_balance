defmodule AccountBalanceWeb.API.CurrentDatePlug do
  @moduledoc """
  A plug to inject current_date into the controllers. In normal environments, it's certainly an
  overkill. Its purpose is fully realized in testing -you can set up date using this plug, without
  the need of using mocks or other means of 'time travel'.
  """

  @behaviour Plug
  alias Plug.Conn

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  def call(conn, opts \\ [])

  def call(%Conn{private: %{"account_balance.assume_date": date}} = conn, _opts) do
    Plug.Conn.assign(conn, :current_date, date)
  end

  def call(conn, _opts) do
    date = DateTime.utc_now() |> DateTime.to_date()
    Plug.Conn.assign(conn, :current_date, date)
  end
end
