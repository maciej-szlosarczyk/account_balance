defmodule AccountBalanceWeb.API.JSONContentTypePlug do
  @moduledoc false

  @behaviour Plug
  alias Plug.Conn

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  def call(conn, _opts) do
    Conn.put_resp_content_type(conn, "application/json")
  end
end
