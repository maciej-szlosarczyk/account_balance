defmodule AccountBalanceWeb.API.AccountView do
  use AccountBalanceWeb, :view

  def render("index.json", %{accounts: accounts}) do
    render_many(accounts, __MODULE__, "show.json")
  end

  def render("show.json", %{account: account}) do
    %{
      "currency" => account.currency,
      "enrollment_id" => account.enrollment_id,
      "id" => account.id,
      "institution" => account.institution,
      "last_four" => account.last_four,
      "links" => %{
        "balances" => Routes.balance_url(AccountBalanceWeb.Endpoint, :index, account.id),
        "details" => Routes.detail_url(AccountBalanceWeb.Endpoint, :index, account.id),
        "self" => Routes.account_url(AccountBalanceWeb.Endpoint, :show, account.id),
        "transactions" => Routes.transaction_url(AccountBalanceWeb.Endpoint, :index, account.id)
      },
      "name" => account.name,
      "subtype" => account.subtype,
      "type" => account.type
    }
  end
end
