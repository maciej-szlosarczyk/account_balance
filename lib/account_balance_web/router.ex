defmodule AccountBalanceWeb.Router do
  use AccountBalanceWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {AccountBalanceWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug AccountBalanceWeb.API.JSONContentTypePlug
    plug AccountBalanceWeb.API.AuthenticationPlug
    plug AccountBalanceWeb.API.CurrentDatePlug
  end

  scope "/", AccountBalanceWeb do
    pipe_through :browser

    get "/", PageController, :index
    live "/metrics", MetricsLive
  end

  scope "/", AccountBalanceWeb.API do
    pipe_through [:api]

    scope "/accounts" do
      scope "/:account_id", Accounts do
        get "/details", DetailController, :index
        get "/balances", BalanceController, :index
        get "/transactions", TransactionController, :index
        get "/transactions/:transaction_id", TransactionController, :show
      end

      get "/:account_id", AccountController, :show
      get "/", AccountController, :index
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", AccountBalanceWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: AccountBalanceWeb.Telemetry
    end
  end
end
